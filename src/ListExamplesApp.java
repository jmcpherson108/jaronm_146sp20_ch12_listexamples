
import java.util.*;

/**
 *
 * @author JaRon
 */
public class ListExamplesApp {
    
    public static void main(String[] args) {
        String[] codesArray = new String[3];
        codesArray[0] = "java";
        codesArray[1] = "jsp";
        codesArray[2] = "mysql";
        for ( String currentCode : codesArray)
        {
            System.out.println( currentCode );
        } // end enhanced for 
        
        // In Java 7 and later, collections can use
        // type inference... (no need to say
        // new ArrayList<String>()
        List<String> codesList = new LinkedList<>();
        codesList.add("java");
        codesList.add("jsp");
        codesList.add("mysql");
        for ( String currentCode : codesList )
        {
            System.out.println( currentCode );
        } // end enhanced for
    } // end method main
    
} // end class ListExamplesApp
